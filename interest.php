
<!DOCTYPE html>
<html>

    <link rel="stylesheet" href="css/stylesheet.css">
    
     <title>Interest Form</title>
  <meta charset="utf-8">

<body>
    
 <ul>
     <li><a href="https://cclsinc.com/"><img src="images/CCLS_Logo-200x75.jpg"></a></li>
 <!--<li style="float:right"><a href="https://cclsinc.com/">Home</a></li>-->
</ul>
    
<div class="container">
    <form  method="post" action="form_process.php">
    <h3>Contact Information</h3>
      
    <div class="form-group">
      <input type="text" name="firstname" value="<?= $firstname ?>" required="required"/>
      <span class="error"><?= $firstname_error ?></span>
      <label class="control-label" for="input">First Name</label><i class="bar"></i>
    </div>
      
    <div class="form-group">
      <input type="text" name="lastname" value="<?= $lastname ?>" required="required"/>
      <label class="control-label" for="input">Last Name</label><i class="bar"></i>
    </div>
      
    <div class="form-group">
      <input type="text" name="phone" value="<?= $phone ?>" required="required"/>
      <span class="error"><?= $phone_error ?></span>
      <label class="control-label" for="input">Phone</label><i class="bar"></i>
    </div>
      
    <div class="form-group">
      <input type="text" name="email" value="<?= $email ?>" required="required"/>
      <span class="error"><?= $email_error ?></span>
      <label class="control-label" for="input">Email</label><i class="bar"></i>
    </div>

    <div class="work_preference" >
        <h3>Work Preference</h3>
        <div class="checkbox">
          <label>
            <input type="checkbox" name="work_preference[]" value="Full Time" checked="checked"/><i class="helper"></i>Full Time
          </label>
        </div>

        <div class="checkbox">
          <label>
            <input type="checkbox" name="work_preference[]" value="Part Time"/><i class="helper"></i>Part Time
          </label>
        </div>

        <div class="checkbox">
          <label>
            <input type="checkbox" name="work_preference[]" value="Weekdays"/><i class="helper"></i>Weekdays
          </label>
        </div>

        <div class="checkbox">
          <label>
            <input type="checkbox" name="work_preference[]" value="Weeknights"/><i class="helper"></i>Weeknights
          </label>
        </div>

        <div class="checkbox">
          <label>
            <input type="checkbox" name="work_preference[]" value="Weekends"/><i class="helper"></i>Weekends
          </label>
        </div>
    </div>
        
    <div class="driverL">
        <h3>Do you have driver's license?</h3>
      <div class="radio">
        <label><input type="radio" name="driverL" checked="checked" value="Yes"/><i class="helper"></i>Yes</label>
      </div>
      <div class="radio">
        <label><input type="radio" name="driverL" value="No"/><i class="helper"></i>No</label>
      </div>
    </div>

   <div class="submit-container">
        <button class="submit" name="submit" type="submit" id="contact-submit" data-submit="...Sending"><span>Submit</span></button>
        <!--<button class="button" name="submit" type="button"><span>Submit</span></button>-->
        <div class="success"><?= $success ?></div>
    </div>
    
  </form>
    

     
  
</div>

</body>

</html>
